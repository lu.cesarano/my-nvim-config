vim.keymap.set('n', '<leader>tt', ':ToggleTerm<CR>', {})
vim.keymap.set('n', '<leader>ee', ':NvimTreeToggle<CR>', {})
vim.keymap.set('t', '<leader>tt', "<C-\\><C-n><C-w>h", {silent = true})

